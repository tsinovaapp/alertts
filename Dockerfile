FROM openjdk:8-jdk-alpine
COPY target/alertsts-*.jar app.jar
EXPOSE 8090
EXPOSE 587
EXPOSE 22
EXPOSE 25
EXPOSE 465
RUN apk update && apk add bash
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar", "-Delasticsearch.url=${elasticsearch_url} -Delasticsearch.username=${elasticsearch_username} -Delasticsearch.password=${elasticsearch_password} -Delasticsearch.index_logs=${elasticsearch_index_logs} -Dspring.mail.host=${smtp_host} -Dspring.mail.port=${smtp_port} -Dspring.mail.username=${smtp_user} -Dspring.mail.password=${smtp_password} -Dsite.url=${site_url} -Dserver.port=8090"]