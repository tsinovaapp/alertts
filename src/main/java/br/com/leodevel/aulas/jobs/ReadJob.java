package br.com.leodevel.aulas.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.leodevel.aulas.client.LogsClient;
import br.com.leodevel.aulas.enums.EmailTemplateEnum;
import br.com.leodevel.aulas.model.GetResponse;
import br.com.leodevel.aulas.model.Log;
import br.com.leodevel.aulas.tasks.LogGeneralToEmail;
import br.com.leodevel.aulas.tasks.LogGeneralToTelegram;
import br.com.leodevel.aulas.tasks.LogNobreakToEmail;

@Component
public class ReadJob {

	private static final Logger LOG = LoggerFactory.getLogger(ReadJob.class);
	
	@Autowired
	private LogsClient logsClient;	
	
	@Autowired
	private LogNobreakToEmail logNobreakEmail;
	
	@Autowired
	private LogGeneralToEmail logGeneralEmail;
	
	@Autowired
	private LogGeneralToTelegram logGeneralTelegram;
	
	@Scheduled(fixedDelay = 5000)
	public void read() {			
		
		try {
			
			GetResponse<Log> getResponse = logsClient
					.search("{\"sort\":[{\"context_date\":{\"order\":\"asc\"}}],\"query\":{\"bool\":{\"should\":[{\"term\":{\"send_alert\":false}},{\"bool\":{\"must_not\":{\"exists\":{\"field\":\"send_alert\"}}}}]}}}");
			
			getResponse.getData().getDocuments().forEach(obj -> {
				
				Log log = obj.getSource();
				
				if (log.getEmailTemplate() == null) {
					return;
				}
				
				boolean sendEmail = true;
				if (log.getEmailTemplate().equalsIgnoreCase(EmailTemplateEnum.nobreak.name())) {
					sendEmail = logNobreakEmail.send(log);
				} else if (log.getEmailTemplate().equalsIgnoreCase(EmailTemplateEnum.general.name())) {
					sendEmail = logGeneralEmail.send(log);
				} else {
					sendEmail = logGeneralEmail.send(log);
				}
				
				boolean sendTelegram = logGeneralTelegram.send(log.getAlertName(), log.getMessage());
				
				if (sendEmail || sendTelegram) {
					LOG.info("Alerta " + obj.getId() + " enviado com sucesso");	
					logsClient.updated(obj.getId(), "{\"doc\":{\"send_alert\":true}}");
				} else {
					LOG.error("Alerta " + obj.getId() + " não foi enviado");	
				}
				
			});			
		
		} catch(Exception ex) {
			LOG.error("Erro ao tentar enviar e-mail", ex);		
		}
		
	}
	
}