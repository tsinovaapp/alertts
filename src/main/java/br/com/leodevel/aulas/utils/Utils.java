package br.com.leodevel.aulas.utils;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.zone.ZoneRules;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class Utils {

	public static String getString(String value) {
		if (value == null) {
			return "";
		}
		return value.trim();
	}
	
	private static ZoneOffset getOffset(TimeZone timeZone, Date date) {
        Calendar dt = Calendar.getInstance(timeZone);
        ZoneId zi = timeZone.toZoneId();
        ZoneRules zr = zi.getRules();
        return zr.getOffset(dt.toInstant());
    }

    private static long getOffsetHours(TimeZone timeZone, Date date) {
        ZoneOffset zo = getOffset(timeZone, date);
        return TimeUnit.SECONDS.toHours(zo.getTotalSeconds());
    }
	
	public static Date getDateTimeClient(Date dateUtc){
        
    	Instant nowUtc = Instant.now();
        ZoneId timeZone = ZoneId.of("America/Sao_Paulo");
        ZonedDateTime nowTimeZone = ZonedDateTime.ofInstant(nowUtc, timeZone);        
        long offset = getOffsetHours(TimeZone.getTimeZone(timeZone), Date.from(nowTimeZone.toInstant()));
        
        Calendar d = Calendar.getInstance();
        d.setTime(dateUtc);
        d.add(Calendar.HOUR, (int) offset);
        
        return d.getTime();
        
    }
	
	public static String dateToString(Date date, String format) {
		return new SimpleDateFormat(format).format(date);
	}
	
}
