package br.com.leodevel.aulas.client;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.leodevel.aulas.FeignClientConfig;
import br.com.leodevel.aulas.model.User;

@FeignClient(url = "${elasticsearch.url}", name = "users", value = "users", configuration = FeignClientConfig.class)
public interface UsersClient {
	
	@GetMapping(value = "/_security/user", consumes = "application/json")
	String findAll();
	
	default List<User> findUsers(){
		
		JSONObject obj = new JSONObject(findAll());	
		
		Iterator<String> usernames = obj.keys();
		List<User> users = new ArrayList<>();
		
		while(usernames.hasNext()) {
			
			String username = usernames.next();							
			JSONObject userJson = obj.getJSONObject(username);
			ObjectMapper objectMapper = new ObjectMapper();		
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			try {
				User user = objectMapper.readValue(userJson.toString(), User.class);
				if (user.getEmail() != null && !user.getEmail().isEmpty()) {
					users.add(user);
				}
			} catch (JsonProcessingException e) {
			}			
		}
		
		return users;
		
	}
	
}
