package br.com.leodevel.aulas.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.leodevel.aulas.FeignClientConfig;
import br.com.leodevel.aulas.model.GetResponse;
import br.com.leodevel.aulas.model.Response;
import br.com.leodevel.aulas.model.Log;

@FeignClient(url = "${elasticsearch.url}/${elasticsearch.index_logs}", name = "logs", value = "logs", configuration = FeignClientConfig.class)
public interface LogsClient {
	
	@GetMapping(value = "/_search", consumes = "application/json")
	GetResponse<Log> search(@RequestBody String body);
	
	@PostMapping(value = "/_update/{id}", consumes = "application/json")
	Response updated(@PathVariable("id") String id, @RequestBody String body);	
	
}