package br.com.leodevel.aulas.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.leodevel.aulas.FeignClientConfig;
import br.com.leodevel.aulas.model.GetResponse;
import br.com.leodevel.aulas.model.Nobreak;

@FeignClient(url = "${elasticsearch.url}/nobreakts*", name = "nobreak", value = "nobreak", configuration = FeignClientConfig.class)
public interface NobreakClient {
	
	@GetMapping(value = "/_search", consumes = "application/json")
	GetResponse<Nobreak> search(@RequestBody String body);
	
}