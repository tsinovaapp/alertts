package br.com.leodevel.aulas.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import br.com.leodevel.aulas.FeignClientConfig;
import br.com.leodevel.aulas.model.GetResponse;
import br.com.leodevel.aulas.model.TelegramGroup;

@FeignClient(url = "${elasticsearch.url}", name = "telegramgroups", value = "telegramgroups", configuration = FeignClientConfig.class)
public interface TelegramGroupsClient {
	
	@GetMapping(value = "/telegram_groups/_search", consumes = "application/json")
	GetResponse<TelegramGroup> findAll();
	
}
