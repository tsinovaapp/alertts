package br.com.leodevel.aulas.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.leodevel.aulas.FeignClientConfig;
import br.com.leodevel.aulas.model.ResponseTelegram;

@FeignClient(url = "${telegram.api}", name = "telegram", value = "telegram", configuration = FeignClientConfig.class)
public interface TelegramClient {
	
	@GetMapping(value = "/{token}/sendMessage", consumes = "application/json")
	ResponseTelegram send(@PathVariable("token") String token, 
			  @RequestParam(name = "chat_id") String chatId,
			  @RequestParam(name = "text") String text);
	
}
