package br.com.leodevel.aulas.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Total {

	private Integer value;
	private String relation;
	
}