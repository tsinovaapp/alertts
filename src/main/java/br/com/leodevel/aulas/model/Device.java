package br.com.leodevel.aulas.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Device {

	@JsonProperty("ip")
	private String ip;
	
}