package br.com.leodevel.aulas.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Nobreak {

	@JsonProperty("device")
	private Device device;
	
	@JsonProperty("model_nobreak")
	private String model;
	
	@JsonProperty("name_nobreak")
	private String name;
	
	@JsonProperty("serial_number_cpu_nobreak")
	private String serial;
	
	@JsonProperty("client_name")
	private String clientName;
	
	@JsonProperty("responsible_name")
	private String responsibleName; // nome responsável
	
	@JsonProperty("responsible_tel")
	private String responsibleTel; // tel responsável
	
	@JsonProperty("responsible_email")
	private String responsibleEmail; // email responsável
	
	@JsonProperty("sector_name")
	private String sectorName; // departamento
	
	@JsonProperty("unit")
	private Unit unit; // unidade
	
	@JsonProperty("connected_devices")
	private List<String> connectedDevices;
	
}