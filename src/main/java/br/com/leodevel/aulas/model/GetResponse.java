package br.com.leodevel.aulas.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetResponse<S> {

	@JsonProperty("hits")
	private Data<S> data;
	
}
