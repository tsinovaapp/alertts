package br.com.leodevel.aulas.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response {

	@JsonProperty("result")
	private String result;
	
	@JsonProperty("_shards")
	private Shard shard;
	
}
