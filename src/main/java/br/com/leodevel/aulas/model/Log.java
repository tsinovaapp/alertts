package br.com.leodevel.aulas.model;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Log {

	@JsonProperty("context_date")
	private LocalDateTime date;
	
	@JsonProperty("alert_name")
	private String alertName;
	
	@JsonProperty("alert_id")
	private String alertId;
	
	@JsonProperty("context_group")
	private String contextGroup;
	
	@JsonProperty("message")
	private String message;
	
	@JsonProperty("plugin")
	private String plugin;
	
	@JsonProperty("alert_type")
	private String alertType;
	
	@JsonProperty("email_template")
	private String emailTemplate;
	
	@JsonProperty("emails")
	private String emails;
	
	@JsonProperty("client_id")
	private Long clientId;
	
	@JsonProperty("client_name")
	private String clientName;
	
}