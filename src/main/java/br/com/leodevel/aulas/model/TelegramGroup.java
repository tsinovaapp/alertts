package br.com.leodevel.aulas.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TelegramGroup {

	private String name;
	private String token;
	@JsonProperty("chat_id")
	private String chatId;
	
}
