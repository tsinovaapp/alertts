package br.com.leodevel.aulas.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Shard {

	@JsonProperty("total")
	private Integer total;
	
	@JsonProperty("successful")
	private Integer successful;
	
	@JsonProperty("failed")
	private Integer failed;
	
}
