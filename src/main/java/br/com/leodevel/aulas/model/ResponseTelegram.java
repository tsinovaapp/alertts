package br.com.leodevel.aulas.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseTelegram {

	private boolean ok;
	
}
