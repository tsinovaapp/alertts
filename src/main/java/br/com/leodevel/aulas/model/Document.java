package br.com.leodevel.aulas.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Document<S> {

	@JsonProperty("_index")
	private String index;
	
	@JsonProperty("_type")
	private String type;
	
	@JsonProperty("_id")
	private String id;
	
	@JsonProperty("_source")
	private S source;
	
}
