package br.com.leodevel.aulas.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Data<S> {

	private Total total;
	
	@JsonProperty("max_score")
	private Double maxScore;
	
	@JsonProperty("hits")
	private List<Document<S>> documents;
	
}
