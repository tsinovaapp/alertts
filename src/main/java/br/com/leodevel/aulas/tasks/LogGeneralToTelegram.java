package br.com.leodevel.aulas.tasks;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.leodevel.aulas.client.TelegramClient;
import br.com.leodevel.aulas.client.TelegramGroupsClient;
import br.com.leodevel.aulas.model.TelegramGroup;

@Service
public class LogGeneralToTelegram {

	private static final Logger LOGGER = LoggerFactory.getLogger(LogGeneralToTelegram.class);
	
	@Autowired
	private TelegramClient telegramClient;
	
	@Autowired
	private TelegramGroupsClient telegramGroupsClient;		
	
	public boolean send(String subject, String message) {
			
		List<TelegramGroup> telegramGroups = telegramGroupsClient.findAll().getData().getDocuments()
				.stream().map(obj -> obj.getSource()).collect(Collectors.toList());	
		
		boolean send = false;
		
		for(TelegramGroup telegramGroup : telegramGroups) {
			
			try {		
				
				send = telegramClient.send("bot" + telegramGroup.getToken(), 
						telegramGroup.getChatId(), "Alerta: " + subject + "\n\n" + message).isOk();
				
			} catch (Exception e) {
				LOGGER.error("Erro ao tentar enviar alerta para o telegram " + telegramGroup.getName(), e);
			}
			
		}
		
		return send;
		
		
	}
	
}
