package br.com.leodevel.aulas.tasks;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Locale;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import br.com.leodevel.aulas.enums.EmailTemplateEnum;
import br.com.leodevel.aulas.model.Log;
import br.com.leodevel.aulas.utils.Utils;

@Service
public class LogGeneralToEmail {

	private static final Logger LOGGER = LoggerFactory.getLogger(LogGeneralToEmail.class);
	
	@Autowired
	private JavaMailSender mailSender;
	
	@Autowired
    private SpringTemplateEngine templateEngine;
	
	@Value("${spring.mail.from}")
	private String smtpFrom;
	
	public boolean send(Log log) {
		
		String []emails = log.getEmails() == null ? new String[] {} : log.getEmails().split(",");
		Context ctx = new Context(Locale.getDefault());
		
		// dados do cliente
	    ctx.setVariable("nome_cliente", Utils.getString(log.getClientName()));
	    
	    // dados do evento
	    ctx.setVariable("nivel", log.getAlertType());
	    ctx.setVariable("nivel_formatted", (log.getAlertType().equalsIgnoreCase("info") ? "NORMAL" :
	    	log.getAlertType().equalsIgnoreCase("critical") ? "CRÍTICO" : log.getAlertType()));
	    ctx.setVariable("descricao", log.getMessage());	    
		
	    // dados enviados para
	    Date dateClient = Utils.getDateTimeClient(new Date());
	    ctx.setVariable("data", Utils.dateToString(dateClient, "dd/MM/yyyy"));
	    ctx.setVariable("horario", Utils.dateToString(dateClient, "HH:mm:ss"));
	    ctx.setVariable("emails", emails);
	    
		try {			
			
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			mimeMessage.setFrom(new InternetAddress(smtpFrom));
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage,
	                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
	                StandardCharsets.UTF_8.name());
		    message.setSubject(log.getAlertName());
		    message.setTo(emails);
			
		    String htmlContent = templateEngine.process(EmailTemplateEnum.general.name() + ".html", ctx);
		    message.setText(htmlContent, true);
			
            mailSender.send(mimeMessage);
			
			return true;
			
		} catch (Exception e) {
			LOGGER.error("Erro ao tentar enviar e-mail", e);
			return false;
		
		}
		
	}
	
}
